<?php
namespace Laukikpatel\SSO;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use App;

class SSOServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     * @param Router $router
     */
    public function boot(Router $router)
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/sso.php', 'sso'
        );

        $this->loadViewsFrom(
            __DIR__.'/view', 'sso'
        );

        $this->publishes([
            __DIR__.'/assets' => public_path('vendor/sso'),
        ], 'public');

        if( version_compare(App::VERSION(), '5.4.0') >= 0 ) {
            $router->aliasMiddleware('sso', 'Laukikpatel\SSO\Middleware\SSOMiddleware');
        } else {
            $router->middleware('sso', 'Laukikpatel\SSO\Middleware\SSOMiddleware');
        }

        $this->loadRoutesFrom(__DIR__.'/routes/web.php');

        view()->share('SyncSSO', Notification::js() );

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}