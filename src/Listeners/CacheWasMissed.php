<?php

namespace Laukikpatel\SSO\Listeners;

use Illuminate\Cache\Events\CacheMissed;
use Cache;

class CacheWasMissed
{

    public static $missed;
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CacheMissed  $event
     * @return void
     */
    public function handle(CacheMissed $event)
    {
        if(in_array($event->key, ['global-header', 'global-footer'])) {

            if(self::$missed) {
                return;
            }

            self::$missed = 1;

            if($adminUri = trim(env('SSO_ADMIN_URL', false), '/')) {
                file_get_contents($adminUri . '/compile-header-footer-html');
            }

            if($event->key == 'global-header') {
                echo Cache::get('global-header');
            }

            if($event->key == 'global-footer') {
                echo Cache::get('global-footer');
            }

        }
    }
}
