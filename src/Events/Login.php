<?php

namespace Laukikpatel\SSO\Events;


class Login
{

    public $user;

    /**
     * Create a new event instance.
     *
     * @param \Laukikpatel\SSO\User $user
     */
    public function __construct( $user )
    {
        $this->user = $user;
    }
}
