<?php
namespace Laukikpatel\SSO;

use Illuminate\Support\Facades\View;

class Notification
{
    public static function getCounter() {
        try {
            $res = Auth::get(trim(config('sso.sso_server_uri'), '/') . '/api/notification/unread-counter');

            if(200 != $res->getStatusCode()) {
                throw new \Exception("Invalid response code");
            }
            return $res->getBody()->getContents();
        } catch (\Exception $e) {
            return 0;
        }
    }

    public static function fetch() {

    }

    public static function html() {
        return View::make('sso::notification', ['counter' => static::getCounter(), 'notifications' => []])->render();
    }

    public static function js() {
        $sid = isset($_COOKIE['sid']) && $_COOKIE['sid'] ? $_COOKIE['sid'] : false;
        if(!$sid) {
            $sid = md5( uniqid( rand(), true ) );
            setcookie( 'sid', $sid, null, null, env( 'SSO_SERVER_SESSION_DOMAIN' ), null, true );
        }
        return View::make('sso::javascript', ['sid' => $sid])->render();
    }
}
