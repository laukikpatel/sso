<?php
return [

    'sso_server_app_key' => env('SSO_SERVER_APP_KEY', 'base64:54wACC9botVG3Z+0R86m8JPhHZVZ6JgP0c6+q/pUIqQ='),

    'sso_server_uri' => env('SSO_SERVER_URI', 'http://accounts.gg.dev/'),

    'sso_server_session_domain' => env('SSO_SERVER_SESSION_DOMAIN', '.globalgarner.com'),

    'sso_server_cookies_name' => env('SSO_SERVER_COOKIES_NAME', 'accounts'),

    'home_page_uri' => env('HOME_PAGE_URI', env('APP_URL')),

    'socket_server_domain' => env('SOCKET_SERVER_DOMAIN'),

    'user_table' => env('USER_TABLE', 'users'),

    'user_id_field' => env('USER_ID_FIELD', 'sso_user_id'),

    'user_model' => env('USER_MODEL', 'App\User'),

];