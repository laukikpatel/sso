<link rel="stylesheet" href="{{ asset('vendor/sso/notification.css') }}">
<script type="text/javascript" src="{{ asset('vendor/sso/notification.js') }}"></script>
@if(config('sso.socket_server_domain'))
<script src="{{ trim(config('sso.socket_server_domain'), '/') }}/socket.io/socket.io.js"></script>
<script>

    $('#notification-toggle').ggNotification({domain: "{{ trim(config('sso.sso_server_uri', null), '/') }}"});

    var socket = io.connect("{{ config('sso.socket_server_domain') }}", {query: "token={{ $sid }}"});
    socket.on('reload', function () {
        window.location.reload();
    });

    @if(Laukikpatel\SSO\Auth::isLoggedIn())
        socket.on('private-App.Notification.User.{{ Laukikpatel\SSO\Auth::user()->user_id }}', function (message) {
            $('#notification-toggle').ggNotification().increaseCounter();

            if($('.gg-notifications-menu .gg-notifications').is(':visible')) {
                message.webNotification.created_at = 'Now';
                $('#notification-toggle').ggNotification().add(message.webNotification);
            }
        });
    @endif
</script>
@endif