<div class="gg-notifications-menu">
    <span id="notification-toggle"><i class="glyphicon glyphicon-bell"></i> <span>Notifications</span> <span class="badge">{{ $counter }}</span></span>
    <div class="gg-notifications">
        <div class="top-arrow"></div>
        <ul class="gg-notifications-list">
            <li class="gg-notifications-item loading">
                Loading...
            </li>
            {{--<li class="gg-notifications-item unread">
                <a href="javascript:;">
                    <p class="title">GG Wallet</p>
                    <p class="body">INR 50 has been credited to you GG Coin Account</p>
                    <p class="timestamp">Today</p>
                </a>
            </li>--}}
        </ul>
        <div class="gg-notifications-footer">
            <a href="javascript:;" id="markAllMessageAsRead">Mark all as read</a> <a href="{{ trim(config('sso.sso_server_uri', null), '/') }}/notifications">View All</a>
        </div>
    </div>
</div>