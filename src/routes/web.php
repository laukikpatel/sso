<?php

use Laukikpatel\SSO\Events\Login;
use Laukikpatel\SSO\Events\Logout;
use Laukikpatel\SSO\Auth;

Route::get('/sso/callback', function(\Illuminate\Http\Request $request) {

    if(Auth::isLoggedIn()) {
        event( new Login(Auth::user()) );
    }

    session(['internal_login' => 1]);

    $next = session('next_page', $request->get('next'));

    if($next) {
        return redirect($next);
    } else {
        return redirect( config('sso.home_page_uri', '/') );
    }

})->name('ssoCallback')->middleware('web');

Route::get('/sso/login', function(\Illuminate\Http\Request $request) {

    if(Auth::isLoggedIn()) {
        return redirect( config('sso.home_page_uri', '/') );
    }

    $redirectBackURI = $request->headers->get('referer');
    if(!$redirectBackURI) {
        $redirectBackURI = config('sso.home_page_uri', '/');
    }
    $url = urlencode( route('ssoCallback', ['next' => $redirectBackURI]) );
    return redirect(trim(config('sso.sso_server_uri'), '/') . '/login?continue=' . $url);

})->name('ssoLogin');

Route::get('/sso/logout', function(\Illuminate\Http\Request $request) {

    if(Auth::isLoggedIn()) {
        event( new Logout(Auth::user()) );
    }

    session(['internal_login' => 0]);

    $redirectBackURI = $request->headers->get('referer');
    if(!$redirectBackURI) {
        $redirectBackURI = config('sso.home_page_uri', '/');
    }
    $url = urlencode( route('ssoCallback', ['next' => $redirectBackURI]) );
    return redirect(trim(config('sso.sso_server_uri'), '/') . '/logout?continue=' . $url);

})->name('ssoLogout')->middleware('web');

Route::get('/user-existence/{user_id}', function(\Illuminate\Http\Request $request, $user_id) {

    try {
        $user = Auth::getOrCreateUserById($user_id);
        return response()->json(['status' => true, 'user' => $user]);
    } catch (\Exception $e) {
        return response()->json(['status' => false, 'error' => $e->getMessage()]);
    }

})->middleware('sso');