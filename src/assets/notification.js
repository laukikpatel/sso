;(function ($, window, document, undefined) {

    var callAjax = function(url, callBackSuccess, callBackError) {
        if(!jQuery.isFunction(callBackSuccess)) {
            var callBackSuccess = function() {}
        }

        if(!jQuery.isFunction(callBackError)) {
            var callBackError = function() {}
        }

        $.ajax({
            type: 'GET',
            url: url,
            crossDomain: true,
            dataType: "json",
            xhrFields: {
                withCredentials: true
            },
            success: callBackSuccess,
            error: callBackError
        });
    };

    var ggNotification = function (element, options) {
        this.element = element;
        this.options = $.extend({domain:null}, options);
        this.init();
    };

    ggNotification.prototype = {
        init: function () {
            var self = this;
            $(this.element).click(function (e) {
                e.stopPropagation();
                $('.gg-notifications').toggle();
                if ($('.gg-notifications-menu .gg-notifications').is(':visible')) {
                    self.showLoading();
                    self.hideFooter();
                    self.fetch();
                }
            });

            $('.gg-notifications').click(function (e) {
                e.stopPropagation();
            });

            /*$('.gg-notifications').on('click', '.gg-notifications-item a.markAsRead', function (e) {
                e.stopPropagation();
            });*/

            $(window).click(function () {
                $('.gg-notifications').hide();
            });

            $('.gg-notifications #markAllMessageAsRead').click(function() {
                self.markAllMessageAsRead();
            });

            $('.gg-notifications').on('click', '.gg-notifications-item.unread a.markAsRead', function() {
                var ele = $(this);
                var notificationEle = $(this).closest('.gg-notifications-item');
                callAjax(self.options.domain + '/api/notification/mark-as-read/'+notificationEle.data('id'), function(res) {
                    notificationEle.removeClass('unread');
                    ele.remove();
                    self.decreaseCounter();
                }, function(err) {
                    $(self.element).trigger('click');
                });
            });
        },

        fetch: function() {
            var self = this;
            callAjax(this.options.domain + '/api/notification?per_page=6', function(res) {
                if(res.total > 0) {
                    $.each(res.data.reverse(), function(index, notification) {
                        self.add(notification);
                    });
                } else {
                    $(self.element).trigger('click');
                }
            }, function() {
                $(self.element).trigger('click');
            });
        },

        markAllMessageAsRead: function() {
            var self = this;
            callAjax(this.options.domain + '/api/notification/mark-as-read', function(res) {
                $('.gg-notifications-menu #notification-toggle span.badge').text(0);
                $('.gg-notifications-menu .gg-notifications-item.unread').removeClass('unread');
            }, function() {
                $(self.element).trigger('click');
            });
        },

        add: function (notification) {
            this.hideLoading();
            this.showFooter();
            $('.gg-notifications-menu .gg-notifications .gg-notifications-list').prepend('<li class="gg-notifications-item'+(notification.read_at == null ? ' unread' : '')+'" data-id="' + notification.id + '"><a href="' + (notification.url != null && notification.url.length > 0 ? notification.url : 'javascript:;') + '"><p class="title">' + notification.title + '</p><p class="body">' + notification.message + '</p></a><p class="timestamp">' + notification.created_at + (notification.read_at == null ? '<a href="javascript:;" class="markAsRead" title="Mark as Read">&nbsp;</a>' : '') + '</p></li>');
        },

        increaseCounter: function () {
            var counterEle = $('.gg-notifications-menu #notification-toggle span.badge');
            counterEle.text(parseInt(counterEle.text()) + 1)
        },

        decreaseCounter: function (notification) {
            var counterEle = $('.gg-notifications-menu #notification-toggle span.badge');
            var counter = parseInt(counterEle.text()) - 1;
            console.log(counter);
            counterEle.text(counter > 0 ? counter : 0);
        },

        showFooter: function() {
            $('.gg-notifications-menu .gg-notifications .gg-notifications-footer').show();
        },

        hideFooter: function() {
            $('.gg-notifications-menu .gg-notifications .gg-notifications-footer').hide();
        },

        showLoading: function() {
            $('.gg-notifications-menu .gg-notifications ul.gg-notifications-list').html('<li class="gg-notifications-item loading" style="display: list-item;">Loading...</li>');
        },

        hideLoading: function() {
            $('.gg-notifications-menu .gg-notifications ul.gg-notifications-list .gg-notifications-item.loading').remove();
        }
    };

    $.fn['ggNotification'] = function (options) {
        var plugin = this.data('ggNotification');
        if (plugin instanceof ggNotification) {
            if (typeof options !== 'undefined') {
                plugin.init(options);
            }
        } else {
            plugin = new ggNotification(this, options);
            this.data('ggNotification', plugin);
        }
        return plugin;
    };
}(jQuery, window, document));