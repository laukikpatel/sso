# SSO #

### How do I get set up? ###

Add bellow lines in your composer.json file to register sso repository for composer

    "repositories": [
        {
            "type": "vcs",
            "url": "https://laukikpatel@bitbucket.org/laukikpatel/sso.git"
        }
    ],
    "require": {
        "laukikpatel/sso": "dev-master#1.*"
    }

After adding above lines run bellow command to install sso package

    composer install
    
After installation completed add bellow line in your config/app.php to register provider

    Laukikpatel\SSO\SSOServiceProvider::class

Publish the assets

    php artisan vendor:publish --tag=public --provider='Laukikpatel\SSO\SSOServiceProvider'

### Environment variable ###

Add bellow lines in your .env file

    SSO_SERVER_APP_KEY=SSO Server Application Key
    SSO_SERVER_URI=https://accounts.globalgarner.com/
    SSO_SERVER_SESSION_DOMAIN=.globalgarner.com
    SSO_SERVER_COOKIES_NAME=accounts
    HOME_PAGE_URI=Your app home page uri after login
    SOCKET_SERVER_DOMAIN=https://nodejs.globalgarner.com
    
    SSO_ADMIN_URL=http://admin.globalgarner.com/
    
    USER_TABLE=users
    USER_ID_FIELD=sso_user_id
    USER_MODEL=App\User

### Middleware ###

Use middleware for check Authentication

**Example:**

    Route::get('/', 'Controller@method')->middleware('sso');
    Route::get('/', 'Controller@method')->middleware('sso:scope1,scope2');
    
    Route::group(['middleware' => ['sso']], function () {
    
        Route::get('/', 'Controller@method');
        Route::get('/xxx', 'Controller@method');
        
    });
    
    Add a bellow function in your model, model will be {.env > USER_MODEL defined}
    /**
     * Create an User
     * @param array $user | user_id, username, reffer_id, mobile, email, fname, lname, gender, dob, city, state, pincode, is_active
     */
    public static function addUser( $user )
    {
        //Insert into your user table
    }

### Events ###

SSO package raises a variety of events during the authentication process. You may attach listeners to these events in your EventServiceProvider:

    /**
    * The event listener mappings for the application.
    *
    * @var array
    */
    protected $listen = [
        'Laukikpatel\SSO\Events\Login' => [
            'App\Listeners\UserWasLogin',
        ],
        'Laukikpatel\SSO\Events\Logout' => [
            'App\Listeners\UserWasLogout',
        ],
        'Illuminate\Cache\Events\CacheMissed' => [
            'Laukikpatel\SSO\Listeners\CacheWasMissed',
        ]
    ];


### Helper ###

    Check User is logged In or not
    Laukikpatel\SSO\Auth::isLoggedIn()
    
    Get Logged User All Data
    Laukikpatel\SSO\Auth::user()

    Get Logged User UserID
    Laukikpatel\SSO\Auth::user()->user_id
    
    Get Login URI
    Laukikpatel\SSO\Auth::loginURI()
    
    Get Logout URI
    Laukikpatel\SSO\Auth::logoutURI()
    
    Make POST Request with SSO
    Laukikpatel\SSO\Auth::post($url, ['field_name' => 'value'])
    
    Make GET Request with SSO
    Laukikpatel\SSO\Auth::get($url)
    
    Make PUT Request with SSO
    Laukikpatel\SSO\Auth::put($url, ['key1' => 'value1'])
    
    Make DELETE Request with SSO
    Laukikpatel\SSO\Auth::delete($url, ['key1' => 'value1'])
    
    Get Or Create User By SSO User ID
    Laukikpatel\SSO\Auth::getOrCreateUserById($sso_user_id)
    
    Add bellow code in your common layout file
    {!! $SyncSSO !!}
    
    Get Notification HTML
    {!! Laukikpatel\SSO\Notification::html() !!}
    
    //Default endpoint user exist or not
    https://your-domain.globalgarner.com/user-existence/{sso_user_id}
    
    Success Response:
    {
        "status": true,
        "user": {
            "xxx": "xxx",
            ......
        }
    }
    
    Error Response:
    {
        "status": false,
        "error": "xxx"
    }

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact